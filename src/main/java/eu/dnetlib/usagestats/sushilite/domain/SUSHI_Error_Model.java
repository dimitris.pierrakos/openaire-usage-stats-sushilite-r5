package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tsampikos on 23/11/2016.
 */
public class SUSHI_Error_Model {
    private String code;
    private String severity;
    private String message;
    private String helpURL;
    private String data;

    public SUSHI_Error_Model() {
    }

    public SUSHI_Error_Model(String code, String severity, String message, String helpURL, String data) {

        this.helpURL = helpURL;

        this.code = code;
        this.severity = severity;
        this.message = message;
        this.data = data;
    }
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Help_URL")
    public String getHelpURL() {
        return helpURL;
    }

    @JsonProperty("Code")
    public String getCode() {
        return code;
    }

    @JsonProperty("Severity")
    public String getSeverity() {
        return severity;
    }

    @JsonProperty("Message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("Data")
    public String getData() {
        return data;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setSeverity(String severity) {
        this.severity = severity;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setHelpURL(String helpURL) {
        this.helpURL = helpURL;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SUSHI_Error_Model{" +
                "code='" + code + '\'' +
                ", severity='" + severity + '\'' +
                ", message='" + message + '\'' +
                ", helpURL='" + helpURL + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
