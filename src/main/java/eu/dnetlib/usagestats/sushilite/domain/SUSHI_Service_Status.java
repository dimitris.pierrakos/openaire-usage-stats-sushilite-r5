/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.dnetlib.usagestats.sushilite.domain;

/**
 *
 * @author dpie
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
@JsonPropertyOrder({"Description", "Service_Active", "Registry_URL", "Note", "Alerts"})
public class SUSHI_Service_Status {

    private String description;
    private boolean serviceActive;
    private String registry_url;
    private String note;
    private List<Alert> alerts = new ArrayList();

    public SUSHI_Service_Status() {
    }

    public SUSHI_Service_Status(String description, boolean serviceActive, String registry_url, String note,List<Alert> alerts) {
        this.description = description;
        this.serviceActive = serviceActive;
        this.registry_url = registry_url;
        this.note = note;
        this.alerts = alerts;
    }

    @JsonProperty("Description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("Service_Active")
    public boolean getServiceActive() {
        return serviceActive;
    }

    @JsonProperty("Registry_URL")
    public String getRegistryURL() {
        return registry_url;
    }

    @JsonProperty("Note")
    public String getNote() {
        return note;
    }

    @JsonProperty("Alerts")
    public List<Alert> getAlerts() {
        return alerts;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setServiceActive(boolean serviceActive) {
        this.serviceActive = serviceActive;
    }

    public void setRegistry_url(String registry_url) {
        this.registry_url = registry_url;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setAlerts(List<Alert> alerts) {
        this.alerts = alerts;
    }

    @Override
    public String toString() {
        return "SUSHI_Service_Status{" +
                "description='" + description + '\'' +
                ", serviceActive=" + serviceActive +
                ", registry_url='" + registry_url + '\'' +
                ", note='" + note + '\'' +
                ", alerts=" + alerts +
                '}';
    }
}
