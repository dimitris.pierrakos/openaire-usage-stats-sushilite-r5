package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpie on 04/01/2021.
 */
@JsonPropertyOrder({"Title", "Item_ID", "Platform", "Publisher", "Publisher_ID", "Data_Type", "Section_Type", "YOP", "Access_Type", "Access_Method", "Performance"})
//old ReportItem class
public class COUNTER_Title_Usage {

    private String title;
    private List<COUNTER_Item_Identifiers> itemIdentifiers = new ArrayList<>();
    private String platform;
    private String publisher;
    private List<COUNTER_Publisher_Identifiers> publisherIdentifiers = new ArrayList<>();
    private String dataType;
    private String sectionType;
    private String yop;
    private String accessType;
    private String accessMethod;
    private List<COUNTER_Item_Performance> itemPerformances = new ArrayList<>();

    public COUNTER_Title_Usage() {
    }

    public COUNTER_Title_Usage(String title, String publisher, String platform, String dataType,
            String accessMethod, String itemName) {
        this.title = title;
        this.platform = platform;
        this.dataType = dataType;
        this.accessMethod = accessMethod;
    }

    @JsonProperty("Title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("Item_ID")
    public List<COUNTER_Item_Identifiers> getItemIdentifiers() {
        return itemIdentifiers;
    }

    @JsonProperty("Platform")
    public String getItemPlatform() {
        return platform;
    }
    
    @JsonProperty("Publisher")
    public String getPublisher() {
        return publisher;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Data_Type")
    public String getDataType() {
        return dataType;
    }
    
    @JsonProperty("Section_Type")
    public String getSectionType() {
        return sectionType;
    }
    
    @JsonProperty("YOP")
    public String getYOP() {
        return yop;
    }    

    @JsonProperty("Access_Type")
    public String getAccessType() {
        return accessType;
    }

    @JsonProperty("Access_Method")
    public String getAccessMethod() {
        return accessMethod;
    }

    @JsonProperty("Performance")
    public List<COUNTER_Item_Performance> getItemPerformances() {
        return itemPerformances;
    }

    public void addIdentifier(COUNTER_Item_Identifiers itemIdentifier) {
        itemIdentifiers.add(itemIdentifier);
    }

    public void addPerformance(COUNTER_Item_Performance itemPerformance) {
        itemPerformances.add(itemPerformance);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setItemIdentifiers(List<COUNTER_Item_Identifiers> itemIdentifiers) {
        this.itemIdentifiers = itemIdentifiers;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setPublisherIdentifiers(List<COUNTER_Publisher_Identifiers> publisherIdentifiers) {
        this.publisherIdentifiers = publisherIdentifiers;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }

    public void setYop(String yop) {
        this.yop = yop;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public void setAccessMethod(String accessMethod) {
        this.accessMethod = accessMethod;
    }

    public void setItemPerformances(List<COUNTER_Item_Performance> itemPerformances) {
        this.itemPerformances = itemPerformances;
    }

    @Override
    public String toString() {
        return "COUNTER_Title_Usage{" +
                "title='" + title + '\'' +
                ", itemIdentifiers=" + itemIdentifiers +
                ", platform='" + platform + '\'' +
                ", publisher='" + publisher + '\'' +
                ", publisherIdentifiers=" + publisherIdentifiers +
                ", dataType='" + dataType + '\'' +
                ", sectionType='" + sectionType + '\'' +
                ", yop='" + yop + '\'' +
                ", accessType='" + accessType + '\'' +
                ", accessMethod='" + accessMethod + '\'' +
                ", itemPerformances=" + itemPerformances +
                '}';
    }
}
