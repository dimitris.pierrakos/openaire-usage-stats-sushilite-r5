package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 31/10/2016.
 */
public class Instance {

    private String metricType;
    private String count;

    public Instance() {
    }

    public Instance(String type, String count) {
        this.metricType = type;
        this.count = count;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("MetricType")
    public String getMetricType() {
            return metricType;
        }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Count")
    public String getCount() {
            return count;
        }

    public void setMetricType(String metricType) {
        this.metricType = metricType;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Instance{" +
                "metricType='" + metricType + '\'' +
                ", count='" + count + '\'' +
                '}';
    }
}

