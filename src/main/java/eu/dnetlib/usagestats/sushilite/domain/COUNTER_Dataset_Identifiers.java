package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dpie on 15/01/2021.
 */
public class COUNTER_Dataset_Identifiers {

    private String type = null;
    private String value = null;

    public COUNTER_Dataset_Identifiers() {
    }

    public COUNTER_Dataset_Identifiers(String type, String value) {
        this.type = type;
        this.value = value;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    @JsonProperty("Value")
    public String getValue() {
        return value;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "COUNTER_Dataset_Identifiers{" +
                "type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
