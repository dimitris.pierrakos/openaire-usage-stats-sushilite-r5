package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
@JsonPropertyOrder({"Dataset_Title", "Publisher", "YOP", "Access_Method", "Performance"})
//old ReportItem class
public class COUNTER_Dataset_Usage {

    private String datasetTitle;
    private List<COUNTER_Dataset_Identifiers> datasetIdentifiers = new ArrayList<>();
    private List<COUNTER_Dataset_Contributors> datasetContributors = new ArrayList<>();
    private List<COUNTER_Dataset_Dates> datasetDates = new ArrayList<>();
    private List<COUNTER_Dataset_Attributes> datasetAttributes = new ArrayList<>();
    private String platform;
    private String publisher;
    private List<COUNTER_Publisher_Identifiers> publisherIdentifiers = new ArrayList<>();
    private COUNTER_Item_Parent itemParent = new COUNTER_Item_Parent();
    private List<COUNTER_Item_Component> itemComponents = new ArrayList<>();
    private String dataType;
    private String yop;
    private String accessMethod;

    private List<COUNTER_Dataset_Performance> datasetPerformances = new ArrayList<>();

    public COUNTER_Dataset_Usage() {
    }

    public COUNTER_Dataset_Usage(String datasetTitle, String publisher, String platform, String dataType,
            String yop, String accessMethod) {
        this.datasetTitle = datasetTitle;
        this.publisher = publisher;
        this.platform = platform;
        this.dataType = dataType;
        this.yop = yop;
        if(accessMethod.equals("Regular"))
            this.accessMethod = "Regular";
        if(accessMethod.equals("regular"))
            this.accessMethod = "Regular";
        if(accessMethod.equals("machine"))
            this.accessMethod = "Machine";
        //this.itemIdentifiers = itemIdentifiers;
        this.datasetContributors = null;
        this.datasetDates = null;
        this.datasetAttributes = null;
        this.publisherIdentifiers = null;
        this.itemParent = null;
        this.itemComponents = null;

    }

    @JsonProperty("Dataset_Title")
    public String getItem() {
        return datasetTitle;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Dataset_ID")
    public List<COUNTER_Dataset_Identifiers> getDatasetIdentifiers() {
        return datasetIdentifiers;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Dataset_Contributors")
    public List<COUNTER_Dataset_Contributors> getDatasetContributors() {
        return datasetContributors;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Dataset_Dates")
    public List<COUNTER_Dataset_Dates> getDatasetDates() {
        return datasetDates;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Dataset_Attributes")
    public List<COUNTER_Dataset_Attributes> getDatasetAttributes() {
        return datasetAttributes;
    }

    @JsonProperty("Platform")
    public String getItemPlatform() {
        return platform;
    }

    @JsonProperty("Publisher")
    public String getItemPPublisher() {
        return publisher;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Publisher_ID")
    public List<COUNTER_Publisher_Identifiers> getPublisherIdentifiers() {
        return publisherIdentifiers;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item_Parent")
    public COUNTER_Item_Parent getItemParent() {
        return itemParent;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item_Component")
    public List<COUNTER_Item_Component> getItemComponents() {
        return itemComponents;
    }

    @JsonProperty("Data_Type")
    public String getDataType() {
        return dataType;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("YOP")
    public String getYop() {
        return yop;
    }

    @JsonProperty("Access_Method")
    public String getAccessMethod() {
        return accessMethod;
    }

    @JsonProperty("Performance")
    public List<COUNTER_Dataset_Performance> getItemPerformances() {
        return datasetPerformances;
    }

    public void addIdentifier(COUNTER_Dataset_Identifiers datasetIdentifier) {
        datasetIdentifiers.add(datasetIdentifier);
    }

    public void setIdentifier(List<COUNTER_Dataset_Identifiers> datasetIdentifier) {
        this.datasetIdentifiers = datasetIdentifier;
    }

    public void addDatasetContributors(COUNTER_Dataset_Contributors datasetContributor) {
        datasetContributors.add(datasetContributor);
    }

    public void addDatasetDates(COUNTER_Dataset_Dates itemDate) {
        datasetDates.add(itemDate);
    }

    public void addDatasetAttributes(COUNTER_Dataset_Attributes datasetAttribute) {
        datasetAttributes.add(datasetAttribute);
    }

    public void addPublisherIdentifiers(COUNTER_Publisher_Identifiers publisherIdentifier) {
        publisherIdentifiers.add(publisherIdentifier);
    }

    public void addItemParent(COUNTER_Item_Parent itemParent) {
        this.itemParent = itemParent;
    }

    public void addItemComponent(COUNTER_Item_Component itemComponent) {
        itemComponents.add(itemComponent);
    }

    public void addPerformance(COUNTER_Dataset_Performance datasetPerformance) {
        datasetPerformances.add(datasetPerformance);
    }

    public void setDatasetTitle(String datasetTitle) {
        this.datasetTitle = datasetTitle;
    }

    public void setDatasetIdentifiers(List<COUNTER_Dataset_Identifiers> datasetIdentifiers) {
        this.datasetIdentifiers = datasetIdentifiers;
    }

    public void setDatasetContributors(List<COUNTER_Dataset_Contributors> datasetContributors) {
        this.datasetContributors = datasetContributors;
    }

    public void setDatasetDates(List<COUNTER_Dataset_Dates> datasetDates) {
        this.datasetDates = datasetDates;
    }

    public void setDatasetAttributes(List<COUNTER_Dataset_Attributes> datasetAttributes) {
        this.datasetAttributes = datasetAttributes;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setPublisherIdentifiers(List<COUNTER_Publisher_Identifiers> publisherIdentifiers) {
        this.publisherIdentifiers = publisherIdentifiers;
    }

    public void setItemParent(COUNTER_Item_Parent itemParent) {
        this.itemParent = itemParent;
    }

    public void setItemComponents(List<COUNTER_Item_Component> itemComponents) {
        this.itemComponents = itemComponents;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setYop(String yop) {
        this.yop = yop;
    }

    public void setAccessMethod(String accessMethod) {
        this.accessMethod = accessMethod;
    }

    public void setDatasetPerformances(List<COUNTER_Dataset_Performance> datasetPerformances) {
        this.datasetPerformances = datasetPerformances;
    }

    @Override
    public String toString() {
        return "COUNTER_Dataset_Usage{" +
                "datasetTitle='" + datasetTitle + '\'' +
                ", datasetIdentifiers=" + datasetIdentifiers +
                ", datasetContributors=" + datasetContributors +
                ", datasetDates=" + datasetDates +
                ", datasetAttributes=" + datasetAttributes +
                ", platform='" + platform + '\'' +
                ", publisher='" + publisher + '\'' +
                ", publisherIdentifiers=" + publisherIdentifiers +
                ", itemParent=" + itemParent +
                ", itemComponents=" + itemComponents +
                ", dataType='" + dataType + '\'' +
                ", yop='" + yop + '\'' +
                ", accessMethod='" + accessMethod + '\'' +
                ", datasetPerformances=" + datasetPerformances +
                '}';
    }
}
