/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.dnetlib.usagestats.sushilite.domain;

/**
 *
 * @author dpie
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
//@JsonPropertyOrder({"Description", "Service_Active", "Registry_URL", "Note", "Alerts"})
public class COUNTER_Dataset_Report {

    private SUSHI_Report_Header_DSR reportHeader;
    private List<COUNTER_Dataset_Usage> reportDatasets = new ArrayList();
    final String createdBy="OpenAIRE UsageCounts Service";

    public COUNTER_Dataset_Report(){

    }

    public COUNTER_Dataset_Report(String created,String customerID, String reportID, String reportName, String insitutionName, List<SUSHI_Org_Identifiers> institutionID, List<SUSHI_Error_Model> exceptions,
            List<Filter> reportFilters,List<COUNTER_Dataset_Usage> reportDatasets) {
        

        this.reportHeader = new SUSHI_Report_Header_DSR(createdBy, customerID, reportID, "5", reportName);
        this.reportDatasets = reportDatasets;

        List<ReportAttribute> reportAttributes=new ArrayList();
        reportAttributes.add(new ReportAttribute("Attributes_To_Show",("Data_Type|Access_Method" )));

        reportHeader.setExceptions(exceptions);
        reportHeader.setCreated(created);
        reportHeader.setReportFiters(reportFilters);
        reportHeader.setReportAttributes(reportAttributes);
        reportHeader.setExceptions(exceptions);
        
    }

    @JsonProperty("Report_Header")
    public SUSHI_Report_Header_DSR getReportHeader() {
        return reportHeader;
    }
    @JsonProperty("Report_Datasets")
    public List<COUNTER_Dataset_Usage> getReportDatasets() {
        return reportDatasets;
    }

    public void setReportPR(SUSHI_Report_Header_DSR reportHeader) {
        this.reportHeader = reportHeader;
    }

    public void setReportHeader(SUSHI_Report_Header_DSR reportHeader) {
        this.reportHeader = reportHeader;
    }

    public void setReportDatasets(List<COUNTER_Dataset_Usage> reportDatasets) {
        this.reportDatasets = reportDatasets;
    }

    @Override
    public String toString() {
        return "COUNTER_Dataset_Report{" +
                "reportHeader=" + reportHeader +
                ", reportDatasets=" + reportDatasets +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
