package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by D.Pierrakos
 */
public class COUNTER_Dataset_Contributors {

    private String type = null;
    private String name = null;
    private String identifier = null;

    public COUNTER_Dataset_Contributors() {
    }

    public COUNTER_Dataset_Contributors(String type, String name, String identifier) {
        this.type = type;
        this.name = name;
        this.identifier = identifier;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }
    
    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Identifier")
    public String getIdentifier() {
        return identifier;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public String toString() {
        return "COUNTER_Dataset_Contributors{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", identifier='" + identifier + '\'' +
                '}';
    }
}
