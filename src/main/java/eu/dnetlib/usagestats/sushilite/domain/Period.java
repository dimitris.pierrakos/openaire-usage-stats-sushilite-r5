package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by D.Pierrakos
 */
public class Period {
    private String begin;
    private String end;

    public Period() {
    }

    public Period(String begin, String end) {
        this.begin = begin;
        this.end = end;
    }

    @JsonProperty("Begin_Date")
    public String getBegin() {
        return begin;
    }

    @JsonProperty("End_Date")
    public String getEnd() {
        return end;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "Period{" +
                "begin='" + begin + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
