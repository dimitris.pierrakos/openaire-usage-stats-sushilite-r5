package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by D.Pierrakos
 */
public class COUNTER_Item_Attributes {
    private String type;
    private String value;

    public COUNTER_Item_Attributes() {
    }

    public COUNTER_Item_Attributes(String type, String value) {
        this.type = type;
        this.value = value;
    }

    @JsonProperty("Type")
    public String getType() {
        return type;
    }

    @JsonProperty("Value")
    public String getValue() {
        return value;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "COUNTER_Item_Attributes{" +
                "type='" + type + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
