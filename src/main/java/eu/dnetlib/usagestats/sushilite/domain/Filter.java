package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 26/10/2016.
 */
public class Filter {
    private String name = null;
    private String value = null;

    public Filter() {
    }

    public Filter(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Value")
    public String getValue() {
        return value;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
