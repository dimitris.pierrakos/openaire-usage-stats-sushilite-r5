package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
@JsonPropertyOrder({"Platform", "Data_Type", "Access_Method", "Performance"})
//old ReportItem class
public class COUNTER_Platform_Usage {

    private String platform;
    private String dataType;
    private String accessMethod;

    private List<COUNTER_Item_Performance> itemPerformances = new ArrayList<>();

    public COUNTER_Platform_Usage() {
    }

    public COUNTER_Platform_Usage(String platform, String dataType, String accessMethod) {
        this.platform = platform;
        this.dataType = dataType;
        this.accessMethod = accessMethod;
    }

    @JsonProperty("Platform")
    public String getItemPlatform() {
        return platform;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Data_Type")
    public String getDataType() {
        return dataType;
    }

    @JsonProperty("Access_Method")
    public String getAccessMethod() {
        return accessMethod;
    }

    @JsonProperty("Performance")
    public List<COUNTER_Item_Performance> getItemPerformances() {
        return itemPerformances;
    }

    public void addPerformance(COUNTER_Item_Performance itemPerformance) {
        itemPerformances.add(itemPerformance);
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setAccessMethod(String accessMethod) {
        this.accessMethod = accessMethod;
    }

    public void setItemPerformances(List<COUNTER_Item_Performance> itemPerformances) {
        this.itemPerformances = itemPerformances;
    }

    @Override
    public String toString() {
        return "COUNTER_Platform_Usage{" +
                "platform='" + platform + '\'' +
                ", dataType='" + dataType + '\'' +
                ", accessMethod='" + accessMethod + '\'' +
                ", itemPerformances=" + itemPerformances +
                '}';
    }
}
