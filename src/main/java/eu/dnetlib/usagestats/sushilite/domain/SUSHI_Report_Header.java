package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by dpie on 25/10/2019.
 */
@JsonPropertyOrder({"Created", "Created_By", "Customer_ID", "Report_ID", "Release", "Report_Name", "Institution_Name", "Institution_ID", "Report_Filters", "Report_Attributes", "Exceptions"})
public class SUSHI_Report_Header {

    private String created;
    private String createdBy;
    private String customerID;
    private String reportID;
    private String release;
    private String reportName;
    private String institutionName;
    private List<SUSHI_Org_Identifiers> institutionID = new ArrayList();
    private List<Filter> reportFiters = new ArrayList();
    private List<ReportAttribute> reportAttributes = new ArrayList();
    private List<SUSHI_Error_Model> exceptions = new ArrayList();
    

    public SUSHI_Report_Header() {
    }

    public SUSHI_Report_Header(String createdBy, String customerID, String reportID,
            String release, String reportName, String institutionName,
            List<SUSHI_Org_Identifiers> institutionID) {
        //this.created = created;
        this.createdBy = createdBy;
        this.customerID = customerID;
        this.reportID = reportID;
        this.release = release;
        this.reportName = reportName;
        this.institutionName = institutionName;
        this.institutionID = institutionID;

    }

    @JsonProperty("Created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("Created_By")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("Customer_ID")
    public String getCustomerID() {
        return customerID;
    }

    @JsonProperty("Report_ID")
    public String getReportID() {
        return reportID;
    }

    @JsonProperty("Report_Name")
    public String getReportName() {
        return reportName;
    }
@JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Institution_Name")
    public String getInstitutionName() {
        return institutionName;
    }
@JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Institution_ID")
    public List<SUSHI_Org_Identifiers> getInstitutionID() {
        return institutionID;
    }

    @JsonProperty("Report_Filters")
    public List<Filter> getReportFilters() {
        return reportFiters;
    }

    @JsonProperty("Report_Attributes")
    public List<ReportAttribute> getReportAttributes() {
        return reportAttributes;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Exceptions")
    public List<SUSHI_Error_Model> getExceptions() {
        return exceptions;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setReportFiters(List<Filter> reportFiters) {
        this.reportFiters = reportFiters;
    }

    public void setReportAttributes(List<ReportAttribute> reportAttributes) {
        this.reportAttributes = reportAttributes;
    }

    public void setExceptions(List<SUSHI_Error_Model> exceptions) {
        this.exceptions = exceptions;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public void setReportID(String reportID) {
        this.reportID = reportID;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public void setInstitutionID(List<SUSHI_Org_Identifiers> institutionID) {
        this.institutionID = institutionID;
    }

    @Override
    public String toString() {
        return "SUSHI_Report_Header{" +
                "created='" + created + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", customerID='" + customerID + '\'' +
                ", reportID='" + reportID + '\'' +
                ", release='" + release + '\'' +
                ", reportName='" + reportName + '\'' +
                ", institutionName='" + institutionName + '\'' +
                ", institutionID=" + institutionID +
                ", reportFiters=" + reportFiters +
                ", reportAttributes=" + reportAttributes +
                ", exceptions=" + exceptions +
                '}';
    }
}
