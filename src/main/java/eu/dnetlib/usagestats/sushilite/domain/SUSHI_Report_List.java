/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.dnetlib.usagestats.sushilite.domain;

/**
 *
 * @author dpie
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
@JsonPropertyOrder({"Report_Name", "Report_ID", "Release", "Report_Description", "Path"})
public class SUSHI_Report_List {

    private String reportName;
    private String reportID;
    private String release;
    private String reportDescription;
    private String path;

    public SUSHI_Report_List() {
    }

    public SUSHI_Report_List(String reportName, String reportID, String release, String reportDescription, String path) {
        this.reportName = reportName;
        this.reportID = reportID;
        this.release = release;
        this.reportDescription = reportDescription;
        this.path = path;
    }

    @JsonProperty("Report_Name")
    public String getreportName() {
        return reportName;
    }

    @JsonProperty("Report_ID")
    public String getReportID() {
        return reportID;
    }

    @JsonProperty("Report_Description")
    public String getReportDescription() {
        return reportDescription;
    }

    @JsonProperty("Path")
    public String getPath() {
        return path;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public void setReportID(String reportID) {
        this.reportID = reportID;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public void setReportDescription(String reportDescription) {
        this.reportDescription = reportDescription;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "SUSHI_Report_List{" +
                "reportName='" + reportName + '\'' +
                ", reportID='" + reportID + '\'' +
                ", release='" + release + '\'' +
                ", reportDescription='" + reportDescription + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
