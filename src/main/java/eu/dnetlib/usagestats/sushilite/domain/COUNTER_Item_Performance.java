package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
public class COUNTER_Item_Performance {

    private Period period;
    private List<Instance> instances = new ArrayList<>();

    public COUNTER_Item_Performance() {
    }

    public COUNTER_Item_Performance(String start, String end, String downloads,
            String views, String uniqueDownloads, String uniqueViews) {
        period = new Period(start, end);
        if (downloads != null && views != null && uniqueDownloads != null && uniqueViews != null) {
            if (!downloads.equals("0")) {
                instances.add(new Instance("Total_Item_Requests", downloads));
            }
            if (!views.equals("0")) {
                instances.add(new Instance("Total_Item_Investigations", views));
            }
            if (!uniqueDownloads.equals("0")) {
                instances.add(new Instance("Unique_Item_Requests", uniqueDownloads));
            }
            if (!uniqueViews.equals("0")) {
                instances.add(new Instance("Unique_Item_Investigations", uniqueViews));
            }
        } else if (downloads != null && views != null && uniqueDownloads != null && uniqueViews == null) {
            if (!downloads.equals("0")) {
                instances.add(new Instance("Total_Item_Requests", downloads));
            }
            if (!views.equals("0")) {
                instances.add(new Instance("Total_Item_Investigations", views));
            }
            if (!uniqueDownloads.equals("0")) {
                instances.add(new Instance("Unique_Item_Requests", uniqueDownloads));
            }
        } else if (downloads != null && views != null && uniqueDownloads == null && uniqueViews != null) {
            if (!downloads.equals("0")) {
                instances.add(new Instance("Total_Item_Requests", downloads));
            }
            if (!views.equals("0")) {
                instances.add(new Instance("Total_Item_Investigations", views));
            }
            if (!uniqueViews.equals("0")) {
                instances.add(new Instance("Unique_Item_Investigations", uniqueViews));
            }
        } else if (downloads != null && views == null && uniqueDownloads != null && uniqueViews != null) {
            if (!downloads.equals("0")) {
                instances.add(new Instance("Total_Item_Requests", downloads));
            }
            if (!uniqueDownloads.equals("0")) {
                instances.add(new Instance("Unique_Item_Requests", uniqueDownloads));
            }
            if (!uniqueViews.equals("0")) {
                instances.add(new Instance("Unique_Item_Investigations", uniqueViews));
            }
        } else if (downloads == null && views != null && uniqueDownloads != null && uniqueViews != null) {
            if (!views.equals("0")) {
                instances.add(new Instance("Total_Item_Investigations", views));
            }
            if (!uniqueDownloads.equals("0")) {
                instances.add(new Instance("Unique_Item_Requests", uniqueDownloads));
            }
            if (!uniqueViews.equals("0")) {
                instances.add(new Instance("Unique_Item_Investigations", uniqueViews));
            }
        } else if (downloads != null && views != null && uniqueDownloads == null && uniqueViews == null) {
            if (!downloads.equals("0")) {
                instances.add(new Instance("Total_Item_Requests", downloads));
            }
            if (!views.equals("0")) {
                instances.add(new Instance("Total_Item_Investigations", views));
            }
        } else if (downloads == null && views == null && uniqueDownloads != null && uniqueViews != null) {
            if (!uniqueDownloads.equals("0")) {
                instances.add(new Instance("Unique_Item_Requests", uniqueDownloads));
            }
            if (!uniqueViews.equals("0")) {
                instances.add(new Instance("Unique_Item_Investigations", uniqueViews));
            }
        } else if (downloads != null && views == null && uniqueDownloads != null && uniqueViews == null) {
            if (!downloads.equals("0")) {
                instances.add(new Instance("Total_Item_Requests", downloads));
            }
            if (!uniqueDownloads.equals("0")) {
                instances.add(new Instance("Unique_Item_Requests", uniqueDownloads));
            }
        } else if (downloads == null && views != null && uniqueDownloads == null && uniqueViews != null) {
            if (!views.equals("0")) {
                instances.add(new Instance("Total_Item_Investigations", views));
            }
            if (!uniqueViews.equals("0")) {
                instances.add(new Instance("Unique_Item_Investigations", uniqueViews));
            }
        } else if (downloads != null && views == null && uniqueDownloads == null && uniqueViews != null) {
            if (!downloads.equals("0")) {
                instances.add(new Instance("Total_Item_Requests", downloads));
            }
            if (!uniqueViews.equals("0")) {
                instances.add(new Instance("Unique_Item_Investigations", uniqueViews));
            }
        } else if (downloads == null && views != null && uniqueDownloads != null && uniqueViews == null) {
            if (!views.equals("0")) {
                instances.add(new Instance("Total_Item_Investigations", views));
            }
            if (!uniqueDownloads.equals("0")) {
                instances.add(new Instance("Unique_Item_Requests", uniqueDownloads));
            }
        } else if (downloads != null && views == null && uniqueDownloads == null && uniqueViews == null) {
            if (!downloads.equals("0")) {
                instances.add(new Instance("Total_Item_Requests", downloads));
            }
        } else if (downloads == null && views != null && uniqueDownloads == null && uniqueViews == null) {
            if (!views.equals("0")) {
                instances.add(new Instance("Total_Item_Investigations", views));
            }
        } else if (downloads == null && views == null && uniqueDownloads != null && uniqueViews == null) {
            if (!uniqueDownloads.equals("0")) {
                instances.add(new Instance("Unique_Item_Requests", uniqueDownloads));
            }
        } else if (downloads == null && views == null && uniqueDownloads == null && uniqueViews != null) {
            if (!uniqueViews.equals("0")) {
                instances.add(new Instance("Unique_Item_Investigations", uniqueViews));
            }
        }
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public void setInstances(List<Instance> instances) {
        this.instances = instances;
    }

    @JsonProperty("Period")
    public Period getPeriod() {
        return period;
    }

    @JsonProperty("Instance")
    public List<Instance> getInstance() {
        return instances;
    }

    @Override
    public String toString() {
        return "COUNTER_Item_Performance{" +
                "period=" + period +
                ", instances=" + instances +
                '}';
    }
}
