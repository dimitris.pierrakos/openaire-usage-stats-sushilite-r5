package eu.dnetlib.usagestats.sushilite.domain;


import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dpie on 28/10/2019.
 */
public class Alert {
    private String dateTime;
    private String alert;

    public Alert() {
    }

    public Alert(String dateTime, String alert) {
        this.dateTime = dateTime;
        this.alert = alert;
    }

    @JsonProperty("Date_Time")
    public String getDateTime() {
        return dateTime;
    }

    @JsonProperty("Alert")
    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "dateTime='" + dateTime + '\'' +
                ", alert='" + alert + '\'' +
                '}';
    }
}
