/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.dnetlib.usagestats.sushilite.domain;

/**
 *
 * @author dpie
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dpie on 23/12/2020.
 */
//@JsonPropertyOrder({"Description", "Service_Active", "Registry_URL", "Note", "Alerts"})
public class COUNTER_Platform_Report {

    private SUSHI_Report_Header reportHeader;
    private List<COUNTER_Platform_Usage> reportItems = new ArrayList();
    final String createdBy="OpenAIRE Usage Counts Service";

    public COUNTER_Platform_Report(){

    }
    public COUNTER_Platform_Report(String created,String customerID, String reportID, String reportName, String insitutionName, List<SUSHI_Org_Identifiers> institutionID, List<SUSHI_Error_Model> exceptions,
            List<Filter> reportFilters,List<COUNTER_Platform_Usage> reportItems) {
        

        this.reportHeader = new SUSHI_Report_Header(createdBy, customerID, reportID, "5", reportName, insitutionName, institutionID);
        this.reportItems = reportItems;

        List<ReportAttribute> reportAttributes=new ArrayList();
        reportAttributes.add(new ReportAttribute("Attributes_To_Show",("Data_Type|Access_Method" )));

        reportHeader.setExceptions(exceptions);
        reportHeader.setCreated(created);
        reportHeader.setReportFiters(reportFilters);
        reportHeader.setReportAttributes(reportAttributes);
        reportHeader.setExceptions(exceptions);
        
    }

    @JsonProperty("Report_Header")
    public SUSHI_Report_Header getReportHeader() {
        return reportHeader;
    }
    @JsonProperty("Report_Items")
    public List<COUNTER_Platform_Usage> getReportItems() {
        return reportItems;
    }

    public void setReportPR(SUSHI_Report_Header reportHeader) {
        this.reportHeader = reportHeader;
    }

    public void setReportHeader(SUSHI_Report_Header reportHeader) {
        this.reportHeader = reportHeader;
    }

    public void setReportItems(List<COUNTER_Platform_Usage> reportItems) {
        this.reportItems = reportItems;
    }

    @Override
    public String toString() {
        return "COUNTER_Platform_Report{" +
                "reportHeader=" + reportHeader +
                ", reportItems=" + reportItems +
                ", createdBy='" + createdBy + '\'' +
                '}';
    }
}
