package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonPropertyOrder({"Report_Name", "Report_ID", "Release", "Report_Description", "Path"})
public class ReportSupported {
    private String report_name;
    private String report_id;
    private String release;
    private String description;
    private String path;

    public ReportSupported(){

    }
    public ReportSupported(String report_name, String report_id, String release, String description, String path) {
        this.report_name = report_name;
        this.report_id = report_id;
        this.release = release;
        this.description = description;
        this.path = path;
    }

    @JsonProperty("Report_Name")
    public String getReport_Name() {
        return report_name;
    }

    @JsonProperty("Report_ID")
    public String getReport_ID() {
        return report_id;
    }

    @JsonProperty("Release")
    public String getRelease() {
        return release;
    }

    @JsonProperty("Report_Description")
    public String getReport_Description() {
        return description;
    }

    @JsonProperty("Path")
    public String getPath() {
        return path;
    }

    public void setReport_name(String report_name) {
        this.report_name = report_name;
    }

    public void setReport_id(String report_id) {
        this.report_id = report_id;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ReportSupported{" +
                "report_name='" + report_name + '\'' +
                ", report_id='" + report_id + '\'' +
                ", release='" + release + '\'' +
                ", description='" + description + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
