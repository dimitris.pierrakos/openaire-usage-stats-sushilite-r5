package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
@JsonPropertyOrder({"Platform", "Data_Type", "Access_Method", "Performance"})
//old ReportItem class
public class COUNTER_Item_Usage {

    private String item;
    private List<COUNTER_Item_Identifiers> itemIdentifiers = new ArrayList<>();
    private List<COUNTER_Item_Contributors> itemContributors = new ArrayList<>();
    private List<COUNTER_Item_Dates> itemDates = new ArrayList<>();
    private List<COUNTER_Item_Attributes> itemAttributes = new ArrayList<>();
    private String platform;
    private String publisher;
    private List<COUNTER_Publisher_Identifiers> publisherIdentifiers = new ArrayList<>();
    private COUNTER_Item_Parent itemParent = new COUNTER_Item_Parent();
    private List<COUNTER_Item_Component> itemComponents = new ArrayList<>();
    private String dataType;
    private String yop;
    private String accessType;
    private String accessMethod;

    private List<COUNTER_Item_Performance> itemPerformances = new ArrayList<>();

    public COUNTER_Item_Usage() {
    }

    public COUNTER_Item_Usage(String item, String publisher, String platform, String dataType,
            String yop, String acccesType, String accessMethod) {
        this.item = item;
        this.publisher = publisher;
        this.platform = platform;
        this.dataType = dataType;
        this.yop = yop;
        this.accessType = acccesType;
        this.accessMethod = accessMethod;
        //this.itemIdentifiers = itemIdentifiers;
        this.itemContributors = null;
        this.itemDates = null;
        this.itemAttributes = null;
        this.publisherIdentifiers = null;
        this.itemParent = null;
        this.itemComponents = null;

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item")
    public String getItem() {
        return item;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item_ID")
    public List<COUNTER_Item_Identifiers> getItemIdentifiers() {
        return itemIdentifiers;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item_Contributors")
    public List<COUNTER_Item_Contributors> getItemContributors() {
        return itemContributors;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item_Dates")
    public List<COUNTER_Item_Dates> getItemDates() {
        return itemDates;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item_Attributes")
    public List<COUNTER_Item_Attributes> getItemAttributes() {
        return itemAttributes;
    }

    @JsonProperty("Platform")
    public String getItemPlatform() {
        return platform;
    }

    @JsonProperty("Publisher")
    public String getItemPPublisher() {
        return publisher;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Publisher_ID")
    public List<COUNTER_Publisher_Identifiers> getPublisherIdentifiers() {
        return publisherIdentifiers;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item_Parent")
    public COUNTER_Item_Parent getItemParent() {
        return itemParent;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Item_Component")
    public List<COUNTER_Item_Component> getItemComponents() {
        return itemComponents;
    }

    @JsonProperty("Data_Type")
    public String getDataType() {
        return dataType;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("YOP")
    public String getYop() {
        return yop;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Access_Type")
    public String getAccessType() {
        return accessType;
    }

    @JsonProperty("Access_Method")
    public String getAccessMethod() {
        return accessMethod;
    }

    @JsonProperty("Performance")
    public List<COUNTER_Item_Performance> getItemPerformances() {
        return itemPerformances;
    }

    public void addIdentifier(COUNTER_Item_Identifiers itemIdentifier) {
        itemIdentifiers.add(itemIdentifier);
    }
    public void setIdentifier(List<COUNTER_Item_Identifiers> itemIdentifier) {
        this.itemIdentifiers=itemIdentifier;
    }
    public void addItemContributors(COUNTER_Item_Contributors itemContributor) {
        itemContributors.add(itemContributor);
    }

    public void addItemDates(COUNTER_Item_Dates itemDate) {
        itemDates.add(itemDate);
    }

    public void addItemAttributes(COUNTER_Item_Attributes itemAttribute) {
        itemAttributes.add(itemAttribute);
    }

    public void addPublisherIdentifiers(COUNTER_Publisher_Identifiers publisherIdentifier) {
        publisherIdentifiers.add(publisherIdentifier);
    }

    public void addItemParent(COUNTER_Item_Parent itemParent) {
        this.itemParent = itemParent;
    }

    public void addItemComponent(COUNTER_Item_Component itemComponent) {
        itemComponents.add(itemComponent);
    }

    public void addPerformance(COUNTER_Item_Performance itemPerformance) {
        itemPerformances.add(itemPerformance);
    }

    public void setItem(String item) {
        this.item = item;
    }

    public void setItemIdentifiers(List<COUNTER_Item_Identifiers> itemIdentifiers) {
        this.itemIdentifiers = itemIdentifiers;
    }

    public void setItemContributors(List<COUNTER_Item_Contributors> itemContributors) {
        this.itemContributors = itemContributors;
    }

    public void setItemDates(List<COUNTER_Item_Dates> itemDates) {
        this.itemDates = itemDates;
    }

    public void setItemAttributes(List<COUNTER_Item_Attributes> itemAttributes) {
        this.itemAttributes = itemAttributes;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public void setPublisherIdentifiers(List<COUNTER_Publisher_Identifiers> publisherIdentifiers) {
        this.publisherIdentifiers = publisherIdentifiers;
    }

    public void setItemParent(COUNTER_Item_Parent itemParent) {
        this.itemParent = itemParent;
    }

    public void setItemComponents(List<COUNTER_Item_Component> itemComponents) {
        this.itemComponents = itemComponents;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setYop(String yop) {
        this.yop = yop;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public void setAccessMethod(String accessMethod) {
        this.accessMethod = accessMethod;
    }

    public void setItemPerformances(List<COUNTER_Item_Performance> itemPerformances) {
        this.itemPerformances = itemPerformances;
    }

    @Override
    public String toString() {
        return "COUNTER_Item_Usage{" +
                "item='" + item + '\'' +
                ", itemIdentifiers=" + itemIdentifiers +
                ", itemContributors=" + itemContributors +
                ", itemDates=" + itemDates +
                ", itemAttributes=" + itemAttributes +
                ", platform='" + platform + '\'' +
                ", publisher='" + publisher + '\'' +
                ", publisherIdentifiers=" + publisherIdentifiers +
                ", itemParent=" + itemParent +
                ", itemComponents=" + itemComponents +
                ", dataType='" + dataType + '\'' +
                ", yop='" + yop + '\'' +
                ", accessType='" + accessType + '\'' +
                ", accessMethod='" + accessMethod + '\'' +
                ", itemPerformances=" + itemPerformances +
                '}';
    }
}
