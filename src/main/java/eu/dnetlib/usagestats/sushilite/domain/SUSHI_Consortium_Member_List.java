package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
@JsonPropertyOrder({"Customer_ID", "Requestor_ID", "Name", "Notes", "Institution_ID"})
public class SUSHI_Consortium_Member_List {

    private String customerID;
    private String requestorID;
    private String name;
    private String notes;
    private List<SUSHI_Org_Identifiers> institutionID = new ArrayList();
    

    public SUSHI_Consortium_Member_List() {
    }

    public SUSHI_Consortium_Member_List(String customerID, String requestorID,
            String name, String notes, List<SUSHI_Org_Identifiers> institutionID) {
        this.customerID = customerID;
        this.requestorID = requestorID;
        this.name = name;
        this.notes = notes;
        this.institutionID = institutionID;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Customer_ID")
    public String getCustomerID() {
        return customerID;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Requestor_ID")
    public String getRequestorID() {
        return requestorID;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Notes")
    public String getNotes() {
        return notes;
    }

    @JsonProperty("Institution_ID")
    public List<SUSHI_Org_Identifiers> getInstitutionID() {
        return institutionID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public void setRequestorID(String requestorID) {
        this.requestorID = requestorID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setInstitutionID(List<SUSHI_Org_Identifiers> institutionID) {
        this.institutionID = institutionID;
    }

    @Override
    public String toString() {
        return "SUSHI_Consortium_Member_List{" +
                "customerID='" + customerID + '\'' +
                ", requestorID='" + requestorID + '\'' +
                ", name='" + name + '\'' +
                ", notes='" + notes + '\'' +
                ", institutionID=" + institutionID +
                '}';
    }
}
