package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by tsampikos on 26/10/2016.
 */
public class UsageDateRange {
    private String begin;
    private String end;

    public UsageDateRange() {
    }

    public UsageDateRange(String begin, String end) {
        this.begin = begin;
        this.end = end;
    }

    @JsonProperty("Begin")
    public String getBegin() {
        return begin;
    }

    @JsonProperty("End")
    public String getEnd() {
        return end;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return "UsageDateRange{" +
                "begin='" + begin + '\'' +
                ", end='" + end + '\'' +
                '}';
    }
}
