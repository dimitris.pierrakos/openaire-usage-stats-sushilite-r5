package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by D.Pierrakos
 */
@JsonPropertyOrder({"Report_Name", "Report_ID", "Release", "Created", "Created_By", "Customer_ID",  "Report_Filters", "Report_Attributes", "Exceptions"})
public class SUSHI_Report_Header_DSR {

    private String created;
    private String createdBy;
    private String customerID;
    private String reportID;
    private String release;
    private String reportName;
    private List<Filter> reportFiters = new ArrayList();
    private List<ReportAttribute> reportAttributes = new ArrayList();
    private List<SUSHI_Error_Model> exceptions = new ArrayList();
    

    public SUSHI_Report_Header_DSR() {
    }

    public SUSHI_Report_Header_DSR(String createdBy, String customerID, String reportID,
            String release, String reportName) {
        //this.created = created;
        this.createdBy = createdBy;
        this.customerID = customerID;
        this.reportID = reportID;
        this.release = release;
        this.reportName = reportName;

    }

    @JsonProperty("Created")
    public String getCreated() {
        return created;
    }

    @JsonProperty("Created_By")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("Customer_ID")
    public String getCustomerID() {
        return customerID;
    }

    @JsonProperty("Report_ID")
    public String getReportID() {
        return reportID;
    }

    @JsonProperty("Report_Name")
    public String getReportName() {
        return reportName;
    }


    @JsonProperty("Report_Filters")
    public List<Filter> getReportFilters() {
        return reportFiters;
    }

    @JsonProperty("Report_Attributes")
    public List<ReportAttribute> getReportAttributes() {
        return reportAttributes;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Exceptions")
    public List<SUSHI_Error_Model> getExceptions() {
        return exceptions;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public void setReportFiters(List<Filter> reportFiters) {
        this.reportFiters = reportFiters;
    }

//    @JsonIgnore
//    public Date getBeginDate() {
//        return beginDate;
//    }
//
//    @JsonIgnore
//    public Date getEndDate() {
//        return endDate;
//    }
//
//    public void setReportAttributes(List<Attribute> reportAttributes) {
//        this.reportAttributes = reportAttributes;
//    }
    public void setReportAttributes(List<ReportAttribute> reportAttributes) {
        this.reportAttributes = reportAttributes;
    }

    public void setExceptions(List<SUSHI_Error_Model> exceptions) {
        this.exceptions = exceptions;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public void setReportID(String reportID) {
        this.reportID = reportID;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    @Override
    public String toString() {
        return "SUSHI_Report_Header_DSR{" +
                "created='" + created + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", customerID='" + customerID + '\'' +
                ", reportID='" + reportID + '\'' +
                ", release='" + release + '\'' +
                ", reportName='" + reportName + '\'' +
                ", reportFiters=" + reportFiters +
                ", reportAttributes=" + reportAttributes +
                ", exceptions=" + exceptions +
                '}';
    }
}
