package eu.dnetlib.usagestats.sushilite.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by D.Pierrakos
 */

@JsonPropertyOrder({"Platform", "Data_Type", "Access_Method", "Performance"})
//old ReportItem class
public class COUNTER_Item_Component {

    private String itemName;    
    private List<COUNTER_Item_Identifiers> itemIdentifiers = new ArrayList<>();
    private List<COUNTER_Item_Contributors> itemContributors = new ArrayList<>();
    private List<COUNTER_Item_Dates> itemDates = new ArrayList<>();
    private List<COUNTER_Item_Attributes> itemAttributes = new ArrayList<>();
    private String dataType;
    private List<COUNTER_Item_Performance> itemPerformance = new ArrayList<>();

    public COUNTER_Item_Component() {
    }

    public COUNTER_Item_Component(String publisher, String platform, String dataType,
            String accessMethod, String itemName) {
        this.dataType = dataType;
        this.itemName = itemName;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("Data_Type")
    public String getDataType() {
        return dataType;
    }

    public void addIdentifier(COUNTER_Item_Identifiers itemIdentifier) {
        itemIdentifiers.add(itemIdentifier);
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public void setItemIdentifiers(List<COUNTER_Item_Identifiers> itemIdentifiers) {
        this.itemIdentifiers = itemIdentifiers;
    }

    public void setItemContributors(List<COUNTER_Item_Contributors> itemContributors) {
        this.itemContributors = itemContributors;
    }

    public void setItemDates(List<COUNTER_Item_Dates> itemDates) {
        this.itemDates = itemDates;
    }

    public void setItemAttributes(List<COUNTER_Item_Attributes> itemAttributes) {
        this.itemAttributes = itemAttributes;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public void setItemPerformance(List<COUNTER_Item_Performance> itemPerformance) {
        this.itemPerformance = itemPerformance;
    }

    @Override
    public String toString() {
        return "COUNTER_Item_Component{" +
                "itemName='" + itemName + '\'' +
                ", itemIdentifiers=" + itemIdentifiers +
                ", itemContributors=" + itemContributors +
                ", itemDates=" + itemDates +
                ", itemAttributes=" + itemAttributes +
                ", dataType='" + dataType + '\'' +
                ", itemPerformance=" + itemPerformance +
                '}';
    }
}
